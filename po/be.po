# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Zmicer Turok <nashtlumach@gmail.com>, 2018
# Zmicer Turok <nashtlumach@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Thunar Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-16 06:30+0100\n"
"PO-Revision-Date: 2020-03-02 08:49+0000\n"
"Last-Translator: Zmicer Turok <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian (http://www.transifex.com/xfce/thunar-plugins/language/be/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: be\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: ../libshares/libshares-util.c:166
msgid "Please, write a name."
msgstr "Калі ласка, упішыце імя."

#. Warn the user
#: ../libshares/libshares-util.c:174
msgid ""
"Share name too long. Some old clients may have problems to access it, "
"continue anyway?"
msgstr "Занадта працяглая назва рэсурса. У некаторых састарэлых кліентаў не атрымаецца падлучыцца да яго, працягнуць?"

#: ../libshares/libshares-util.c:187
#, c-format
msgid "Error while getting share information: %s"
msgstr "Падчас атрымання звестак пра рэсурс адбылася памылка: %s"

#: ../libshares/libshares-util.c:197
msgid "Another share has the same name"
msgstr "Іншы рэсурс ужо мае такую назву"

#: ../libshares/libshares-util.c:251
msgid "Cannot modify the share:"
msgstr "Не атрымалася змяніць:"

#: ../libshares/libshares-util.c:353
msgid ""
"Thunar needs to add some permissions to your folder in order to share it. Do"
" you agree?"
msgstr "Thunar мае патрэбу дадаць пэўныя правы доступу да гэтага каталога. Вы згодныя?"

#: ../libshares/libshares-util.c:392
msgid "Error when changing folder permissions."
msgstr "Падчас змены дазволаў на доступ адбылася памылка."

#: ../libshares/shares.c:165
#, c-format
msgid "%s %s %s returned with signal %d"
msgstr "%s %s %s вярнуў сігнал %d"

#: ../libshares/shares.c:174
#, c-format
msgid "%s %s %s failed for an unknown reason"
msgstr "Невядомая памылка падчас запуску %s %s %s"

#: ../libshares/shares.c:195
#, c-format
msgid "'net usershare' returned error %d: %s"
msgstr "Падчас запуску 'net usershare' адбылася памылка %d: %s"

#: ../libshares/shares.c:197
#, c-format
msgid "'net usershare' returned error %d"
msgstr "Падчас запуску 'net usershare' адбылася памылка %d"

#: ../libshares/shares.c:231
#, c-format
msgid "the output of 'net usershare' is not in valid UTF-8 encoding"
msgstr "вывад 'net usershare' мае хібнае кадаванне UTF-8"

#: ../libshares/shares.c:490 ../libshares/shares.c:702
#, c-format
msgid "Failed"
msgstr "Памылка"

#: ../libshares/shares.c:592
#, c-format
msgid "Samba's testparm returned with signal %d"
msgstr "Testparm вярнуўся з сігналам %d"

#: ../libshares/shares.c:598
#, c-format
msgid "Samba's testparm failed for an unknown reason"
msgstr "Падчас запуску testparm Samba адбылася невядомая памылка"

#: ../libshares/shares.c:613
#, c-format
msgid "Samba's testparm returned error %d: %s"
msgstr "Падчас запуску testparm Samba адбылася памылка %d: %s"

#: ../libshares/shares.c:615
#, c-format
msgid "Samba's testparm returned error %d"
msgstr "Падчас запуску testparm Samba адбылася памылка %d"

#: ../libshares/shares.c:784
#, c-format
msgid "Cannot remove the share for path %s: that path is not shared"
msgstr "Не атрымалася выдаліць рэсурс для каталога%s: каталога няма ў агульным доступе"

#: ../libshares/shares.c:837
#, c-format
msgid ""
"Cannot change the path of an existing share; please remove the old share "
"first and add a new one"
msgstr "Не атрымалася змяніць шлях да існага рэсурса; калі ласка, выдаліце стары рэсурс і дадайце новы"

#: ../thunar-plugin/tsp-page.c:151
msgid "<b>Folder Sharing</b>"
msgstr "<b>Агульны доступ да каталогаў</b>"

#. Share check button
#: ../thunar-plugin/tsp-page.c:161
msgid "Share this folder"
msgstr "Дазволіць доступ да гэтага каталога"

#: ../thunar-plugin/tsp-page.c:169
msgid "Share Name:"
msgstr "Сеткавая назва:"

#. Write access
#: ../thunar-plugin/tsp-page.c:180
msgid "Allow others users to write in this folder"
msgstr "Дазволіць іншым карыстальнікам запісваць у гэты каталог"

#. Guest access
#: ../thunar-plugin/tsp-page.c:185
msgid "Allow Guest access"
msgstr "Дазволіць гасцявы доступ"

#: ../thunar-plugin/tsp-page.c:193
msgid "Comments:"
msgstr "Каментары:"

#: ../thunar-plugin/tsp-page.c:209
msgid "_Apply"
msgstr "_Ужыць"

#: ../thunar-plugin/tsp-page.c:298
msgid "Share"
msgstr "Супольныя файлы"

#: ../thunar-plugin/tsp-page.c:370
msgid "You are not the owner of the folder."
msgstr "Вы не ўладальнік гэтага каталога."

#: ../thunar-plugin/tsp-page.c:378
msgid ""
"You may need to install Samba, check your user permissions(usershares group) and re-login.\n"
"<b>More info:</b> <u>http://thunar-shares.googlecode.com/</u>"
msgstr "Вам неабходна будзе ўсталяваць Samba, праверыць дазволы (usershares group) і зноў увайсці ў сістэму.\n<b>Больш падрабязна:</b> <u>http://thunar-shares.googlecode.com/</u>"
